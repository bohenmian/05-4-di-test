package com.twuc.webApp.contract;

/**
 * 代表了一个错误信息。我们希望在出现任何错误的时候返回统一的信息。
 */
public class ErrorMessage {
    private final String message;

    /**
     * 创建一个错误信息的实例。
     *
     * @param message 错误信息的具体细节。
     */
    public ErrorMessage(String message) {
        this.message = message;
    }

    /**
     * 得到错误信息的细节。
     *
     * @return 错误信息的细节。
     */
    public String getMessage() {
        return message;
    }
}
